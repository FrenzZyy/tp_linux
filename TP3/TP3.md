# TP3 : Your own shiet

# Sommaire
- ``I. Installation et configuration``
    - ``1. Installation et config d'Apache ``
    - ``2. Installation et config de Nextcloud et MariaDB``
    - ``3. Installation et config de netdata``
- ``II.  Maintien en conditon operationnelle``
    - ``1. Configuration des alertes``
    - ``2. Backup ``
        - ``2.1 Partage NFS``
        - ``2.2 Backup automatique``


## I. Installation et configuration

Avant de commencer vous aurez besoin de 4 VM (ce sont des VM rocky linux.(Vous n'etes pas obliger d'avoir des VM si vous possedez 4 pc)) donc je vous met le setup de chaque VM ci dessous  : 

- je vous preciserai a chaque fois sur quel VM on va travailler

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp3.linux`    | `10.102.1.11` | Serveur Web             | `80` `22`           | `10.102.1.1` `10.102.1.14`             |
| `db.tp3.linux`     | `10.102.1.12` | Serveur Base de Données | `3306` `22`          | `10.102.1.1` `10.102.1.11`            |
| `backup.tp3.linux` | `10.102.1.13` | Serveur de Backup (NFS) | `2049` `22`          | `10.102.1.1` `10.102.1.11` `10.102.1.12`            |
| `front.tp3.linux`  | `10.102.1.14` | Reverse Proxy           | `80` `22`           | `10.102.1.1` `10.102.1.0/24`             |

Et aussi la config qui faut **``sur chaque VM``** :

A chaque machine déployée, vous DEVREZ vérifier la 📝checklist📝 :

- IP locale, statique ou dynamique
- hostname défini
- firewall actif, qui ne laisse passer que le strict nécessaire
- SSH fonctionnel avec un échange de clé
- accès Internet (une route par défaut, une carte NAT c'est très bien)
- résolution de nom
- résolution de noms publics, en ajoutant un DNS public à la machine
- résolution des noms du TP, à l'aide du fichier /etc/hosts
- monitoring (oui, toutes les machines devront être surveillées)

### 1. Installation et config d'Apache

Sur la VM ``web.tp3.linux``

Tout d'abord il faut installer le paquet httpd avec la commande ``sudo dnf -y install httpd``
*Pour info le fichier de conf principal est /etc/httpd/conf/httpd.conf*

Une fois installer utiliser la commande ``systemctl enable httpd.service`` pour que Apache ce lance automatiquement au demarrage

Ensuite autorisé le port sur lequel tourne Apache dans le firewall avec la commande :  ``sudo firewall-cmd --add-port 80/tcp --permanent``

### 2. Installation et config de Nextcloud et MariaDB

Sur la VM ``web.tp3.linux``

Pour installer nextcloud il suffit de suivre la doc suivante [**doc officielle Rocky Linux** ](https://docs.rockylinux.org/guides/cms/cloud_server_using_nextcloud/)

commande pour nextcloud:

```
[admin@web ~]$ sudo dnf install epel-release -y
[admin@web ~]$ sudo dnf update -y
[admin@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm -y
[admin@web ~]$ sudo dnf module enable php:remi-7.4 -y
[admin@web ~]$ sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp -y
[admin@web ~]$ sudo mkdir /etc/httpd/sites-available
[admin@web ~]$ sudo nano /etc/httpd/sites-available/linux.tp3.web
[admin@web ~]$ sudo mkdir /etc/httpd/sites-enabled
[admin@web ~]$ sudo ln -s /etc/httpd/sites-available/linux.tp3.web /etc/httpd/sites-enabled/
[admin@web ~]$ sudo nano /etc/opt/remi/php74/php.ini
[admin@web ~]$ sudo wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
[admin@web ~]$ sudo unzip nextcloud-21.0.1.zip
[admin@web ~]$ cd nextcloud/
[admin@web ~]$ sudo cp -Rf * /var/www/sub-domains/linux.tp3.web/html/
[admin@web ~]$ sudo chown -Rf apache.apache /var/www/sub-domains/linux.tp3.web/html/
[admin@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[admin@web ~]$ reboot
```

il faut juste intaller la db sur une autre VM

commande pour MariaDB :

[admin@web ~]$ sudo dnf install mariadb-server
[admin@web ~]$ sudo systemctl enable mariadb
[admin@web ~]$ sudo systemctl start mariadb
[admin@web ~]$ sudo systemctl status mariadb
[admin@web ~]$ sudo mysql -u root
[admin@web ~]$ ip a
[admin@web ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
[admin@web ~]$ sudo firewall-cmd --reload

petit point a precisé il faut ajouter la ligne ci-dessous dans le fichier de conf ``/etc/httpd/conf/httpd.conf`` sinon ça ne fonctionnera pas
```
IncludeOptional sites-enabled/*
```

Ensuite il faut se rendre sur internet a l'adresse IP de votre VM web sur le port 80 : ``http://10.102.1.11:80``

Apres cliquez sur "Storage & Database" juste en dessous

choisissez "MySQL/MariaDB"
saisissez les informations pour que NextCloud puisse se connecter avec votre base

saisissez l'identifiant et le mot de passe admin que vous voulez, et validez l'installation

### 3. Installation et config de netdata

Sur la VM ``web.tp3.linux``

Tout d'abord, il faut installer netdata. Pour ce faire, vous allez devoir suivre les instructions : 

* Passez en mode root `su -`
* Executer cette commande `bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)`
* Quitter le mode root `exit`

 **`WARNING`** : Pour modifier la configuration de netdata il faudra se rendre dans le dossier /opt/netdata/etc/netdata.

Une fois l'installation finie, activer le service netdata au démarrage : `systemctl enable netdata` 

ensuite il faut autorisé le port sur lequel tourne netdata dans le firewal pour ce faire on utilise les commande suivante : 

```
[admin@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[admin@web ~]$ sudo firewall-cmd --reload
success
[admin@web ~]$ sudo firewall-cmd --list-all
public (active)
{...}
  ports: 80/tcp 19999/tcp
{...}
```
---

## II.  Maintien en conditon operationnelle

### 1. configuration-des-alertes

---

Avant toute chose vous allez devoir crée un server discord, donc rendez vous sur discord tout en bas de vos server il y a un + cliquez dessus et faites `crée le mien` puis `Pour des amis et moi` puis choissez le nom (le nom n'a pas d'importance). 

Maintenant nous allons parametrer des alertes que l'on va recevoir sur discord.

Tout d'abord on va éditer le fichier `health_alarm_notify.conf` donc on utilise la commande `sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf`.

Ensuite a l'intérieur du fichier il faut trouver les lignes suivantes (on peut rechercher un mot en faisant `/mot_recherché`).

Une fois les lignes trouvé il suffit de les modifier : 
- `SEND_DISCORD` il faut su'il y ai ecrit `YES` comme ci-dessous 
- `DISCORD_WEBHOOK_URL` il faut se rendre dans les parametre de votre server discord puis `Intergration` --> ``webhooks`` --> ``nouveau webhook`` et maintenant copier le lien et coller le sur la ligne ``DISCORD_WEBHOOK_URL=``
- ``DEFAULT_RECIPIENT_DISCORD`` il faut ici mettre un role si vous n'en mettez pas aucun role ne vous enverra de notification mais ce n'ai pas grave

exemple ci-dessous
```
[admin@backup ~]$ sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook

DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/902190313894977577/yOzOmnuQJh_CNFHImPI-ZYJGVHUEfJvo7xWmqGeph1RTGX_akvoelS7G1SPmljfekoLT"

# used to send a notification to a discord role
DEFAULT_RECIPIENT_DISCORD="alarms"
```

Ensuite on va tester pour etre sur que sa marche donc on vas executer les commandes suivante :

```
[admin@backup ~]$ sudo su -s /bin/bash netdata

bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1

bash-4.4$ /opt/netdata//usr/libexec/netdata/plugins.d/alarm-notify.sh test
```
voila ce que sa nous envoie donc cela prouve bien que les alertes son operatinnelle

![](https://i.imgur.com/HTYOHrP.png)

---

## 2. Backup

### 2.1 Partage NFS

Sur la VM ``Backup.tp3.linux``

On va crée un serveur de stockage il hébergera les sauvegardes de tout le monde ce sera un partage NFS

Premierement nous devons crée un dossier ou sera stocker tout no sauvegarde il se trouvera dans /srv : 

```
[admin@backup srv]$ mkdir backups
[admin@backup srv]$ cd backups/
[admin@backup backups]$ sudo mkdir web.tp3.linux
[admin@backup backups]$ ls
web.tp3.linux
```
Maintenant nous allons configurer le partge NFS

Dans le fichier de config ``idmapd.conf`` il suffit de changer le nom de domaine et d'enlever la ligne en tant que commentaire (enlever le #) comme ci-dessous

```
[admin@backup backups]$ sudo dnf -y install nfs-utils
[admin@backup backups]$ sudo nano /etc/idmapd.conf

[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = tp3.linux
```

Ensuite dans le fichier ``export`` on lui dit juste quel dossier on veut partager.

```
[admin@backup backups]$ sudo nano /etc/exports

# create new
# for example, set [/home/nfsshare] as NFS share
/srv/backups/web.tp3.linux 10.102.1.0/24(rw,no_root_squash)
```
ensuite on lance le service NFS

```
[admin@backup ~]$ sudo systemctl start nfs-server.service
```

il faut monter le dossier `/srv/backups/web.tp3.linux` du serveur NFS dans le dossier `/srv/backup/` du serveur Web

Pour setup le point de montage, pour cela nous allons utilise le commande suivantes :

```
[admin@web srv]$ sudo mount -t nfs 10.102.1.13:/srv/backups/web.tp3.linux /srv/backups
```
la commande ``df -ht`` sert juste a verifier que sa a bien crée le point de montage

```
[admin@web srv]$ df -hT
Filesystem                             Type      Size  Used Avail Use% Mounted on
devtmpfs                               devtmpfs  387M     0  387M   0% /dev
tmpfs                                  tmpfs     405M  340K  405M   1% /dev/shm
tmpfs                                  tmpfs     405M  5.6M  400M   2% /run
tmpfs                                  tmpfs     405M     0  405M   0% /sys/fs/cgroup
/dev/mapper/rl-root                    xfs       6.2G  3.4G  2.9G  55% /
/dev/sda1                              xfs      1014M  241M  774M  24% /boot
tmpfs                                  tmpfs      81M     0   81M   0% /run/user/1000
10.102.1.13:/srv/backups/web.tp3.linux nfs4      6.2G  2.2G  4.0G  36% /srv/backups
```
Maintenant nous allons faire en sorte que le point de montage ce configure tout seul au demarrage on va editer le fichier ``fstab`` en mettant les meme information que dans la commande ``sudo mount -t nfs 10.102.1.13:/srv/backups/web.tp3.linux /srv/backups`` a la fin du fichier

```
[admin@web backups]$ sudo nano /etc/fstab
10.102.1.13:/srv/backups/web.tp3.linux /srv/backups                     nfs     defaults        00
```

### 2.2 Backup automatique

Pour faire en sorte que la Backup s'execute automatique on va crée un script en bash

- VOIR **``Script_Backup.sh``**

ci-dessous vous pouvez voir que le script fonctionne correctement : 

```
[admin@backup srv]$ sudo ./tp3_backup.sh backups/ /tmp/test/
tar: Removing leading `/' from member names
archive tp3_backup_211120_191235.tar.gz a été crée
l'archive tp3_backup_211120_191235.tar.gz a été déplacer vers backups/
[OK] Dossier backups/ nettoyer pour garder seulement les 5 sauvegardes les plus récentes.

[admin@backup srv]$ cd backups/
[admin@backup backups]$ ls
tp3_backup_211120_191235.tar.gz  web.tp3.linux
```
---

Maintenant que nous avons un script fonctionnelle, on va faire en sorte qu'il s'execute tout seul a intervalle regulier. 

Pour cela nous allons crée une unité de service.

Tout d'abord il faut ce rendre dans le dossier ``/etc/systemd/system/``, puis executer la commande suivante ``sudo nano tp3_backup.service`` qui va crée un fichier qu'on va modifier.

A l'interieur de ce fichier aujouter le lignes ci dessous

```
[admin@backup system]$ sudo nano tp3_backup.service
[Unit]
Description=Our own lil backup service (tp3)

[Service]
ExecStart=/bin/bash /srv/tp3_backup.sh /srv/backups/ /tmp/test
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=default.target
```

Une fois cela realiser on va tester le fonctionnement pour cela on va utiliser les commandes ci-dessous :

```
[admin@backup system]$ sudo systemctl daemon-reload
[admin@backup system]$ sudo systemctl start tp3_backup.service
[admin@backup system]$ sudo systemctl status tp3_backup.service

{...}
Oct 26 12:03:41 backup.tp3.linux systemd[1]: Starting Our own lil backup service (tp3)...
Oct 26 12:03:41 backup.tp3.linux bash[2549]: tar: Removing leading `/' from member names
Oct 26 12:03:41 backup.tp3.linux bash[2549]: archive tp3_backup_211026_120341.tar.gz a été crée
Oct 26 12:03:41 backup.tp3.linux bash[2549]: l'archive tp3_backup_211026_120341.tar.gz a été déplacer vers /srv/backups/
Oct 26 12:03:41 backup.tp3.linux bash[2549]: [OK] Dossier /srv/backups/ nettoyer pour garder seulement les 5 sauvegarde>
Oct 26 12:03:41 backup.tp3.linux systemd[1]: tp3_backup.service: Succeeded.
Oct 26 12:03:41 backup.tp3.linux systemd[1]: Started Our own lil backup service (tp3).

[admin@backup system]$ cd /srv/backups/
[admin@backup backups]$ ls
tp3_backup_211026_112935.tar.gz  tp3_backup_211026_120341.tar.gz  web.tp3.linux
```

on voit bien qu'on a un fichier de sauvegarde dans ``/srv/backups`` donc tout a bien fonctionner.

---

Maintenant on souhaite que le service ce lance tout seul a intervalle regulier pour cela on crée un timer.

Donc on se rend a nouveau dans ``/etc/systemd/system/`` et on crée un fichier ``tp3_backup.timer``, et on va lui integrer les lignes suivantes :
```
[Unit]
Description=Periodically run our TP3 backup script
Requires=tp3_backup.service

[Timer]
Unit=tp3_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```

```
[admin@web backup]$ sudo systemctl status tp3_backup.timer

tp3_backup.timer - Periodically run our TP3 backup script
   Loaded: loaded (/etc/systemd/system/tp3_backup.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Tue 2021-11-21 20:08:12 CEST; 34s ago
  Trigger: Tue 2021-11-21 20:09:00 CEST; 12s left

nov 21 20:08:12 web.tp3.linux systemd[1]: Started Periodically run our TP3 backup script.
```
on voit bien que le script c'est lancé automatiquement.