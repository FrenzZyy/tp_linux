# TP1 : (re)Familiaration avec un système GNU/Linux
## 0. Préparation de la machine

-  **Setup de deux machines Rocky Linux configurées de façon basique.**

    - un accès internet (via la carte NAT)
```
[admin@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=111 time=121 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=111 time=41.6 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=111 time=38.4 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=111 time=41.1 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3008ms
rtt min/avg/max/mdev = 38.438/60.549/121.130/34.996 ms
```

```
[admin@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=111 time=57.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=111 time=30.4 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=111 time=124 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=111 time=43.4 ms
64 bytes from 8.8.8.8: icmp_seq=5 ttl=111 time=75.4 ms
^C
--- 8.8.8.8 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4008ms
rtt min/avg/max/mdev = 30.447/66.140/123.827/32.478 ms
```

- 
    - un accès à un réseau local

![](https://i.imgur.com/bjPqhAf.png)

    
```
[admin@node1 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8

TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPADDR=10.101.1.11
NETMASK=255.255.255.0
DNS1=1.1.1.1
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s8
UUID=f5ec2a49-4b10-499a-8575-f28349c2d537
DEVICE=enp0s8
ONBOOT=yes
```

```
[admin@node2 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8

TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPADDR=10.101.1.12
NETMASK=255.255.255.0
DNS1=1.1.1.1
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s8
UUID=f5ec2a49-4b10-499a-8575-f28349c2d537
DEVICE=enp0s8
ONBOOT=yes
```

```
[admin@node1 ~]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=1.96 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=1.07 ms
64 bytes from 10.101.1.12: icmp_seq=3 ttl=64 time=0.741 ms
64 bytes from 10.101.1.12: icmp_seq=4 ttl=64 time=1.29 ms
^C
--- 10.101.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 0.741/1.263/1.959/0.446 ms
```

- les machines doivent avoir un nom

```
[admin@node1 ~]$ sudo nano /etc/hostname

node1.tp1.b2
```

```
[admin@node2 ~]$ sudo nano /etc/hostname

node2.tp1.b2
```

- utiliser 1.1.1.1 comme serveur DNS

```
[admin@node1 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 16136
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               8246    IN      A       92.243.16.143

;; Query time: 39 msec
;; SERVER: 192.168.1.1#53(192.168.1.1)
;; WHEN: Sun Sep 26 21:15:58 CEST 2021
;; MSG SIZE  rcvd: 53

- les machines doivent pouvoir se joindre par leurs noms respectifs
```

```
[admin@node2 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 26656
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               8145    IN      A       92.243.16.143

;; Query time: 6 msec
;; SERVER: 192.168.1.1#53(192.168.1.1)
;; WHEN: Sun Sep 26 21:17:38 CEST 2021
;; MSG SIZE  rcvd: 53
```

l'adresse de ynov.com est 92.243.16.143 et l'adresse du serveur est 192.168.1.1

- les machines doivent pouvoir se joindre par leurs noms respectifs

```
[admin@node1 ~]$ sudo nano /etc/hosts

127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
10.101.1.12 node2.tp1.b2
```

```
[admin@node1 ~]$ ping node2.tp1.b2

PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=2.86 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=1.04 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=3 ttl=64 time=0.747 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=4 ttl=64 time=0.684 ms
^X64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=5 ttl=64 time=0.926 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=6 ttl=64 time=1.33 ms
^C
--- node2.tp1.b2 ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5011ms
rtt min/avg/max/mdev = 0.684/1.264/2.859/0.744 ms
```

```
[admin@node2 ~]$ sudo nano /etc/hosts

127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
10.101.1.11 node1.tp1.b2
```

```
[admin@node2 ~]$ ping node1.tp1.b2

PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=1.28 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=2 ttl=64 time=0.712 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=3 ttl=64 time=1.00 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=4 ttl=64 time=1.03 ms
^C
--- node1.tp1.b2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 0.712/1.006/1.282/0.205 ms
```

## I. Utilisateurs

### 1. Création et configuration

```
[admin@node1 ~]$ sudo useradd martial -d /home/ -s /bin/bash -u 100
[sudo] password for admin:
```
```
Not copying any file from skel directory into it.
[admin@node1 ~]$ cat /etc/passwd
{...}
martial:x:100:1001::/home/:/bin/bash
```

```
[admin@node2 ~]$ sudo useradd martial2 -d /home/ -s /bin/bash -u 100
[sudo] password for admin:
```

```
Not copying any file from skel directory into it.
[admin@node2 ~]$ cat /etc/passwd
{...}
martial2:x:100:1001::/home/:/bin/bash
```

- Créer un nouveau groupe admins qui contiendra les utilisateurs de la machine ayant accès aux droits de root via la commande sudo.

```
[admin@node1 ~]$ sudo groupadd admins
[admin@node1 ~]$ sudo visudo
%admins ALL=(ALL) ALL
```

-  Ajouter votre utilisateur à ce groupe admins

```
[admin@node1 ~]$ sudo usermod -aG admins martial
[admin@node1 ~]$ groups martial
martial : martial admins
```

- le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires

```
[admin@node1 usr]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

### 2. SSH

```
C:\Users\marti> ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\marti/.ssh/id_rsa):
C:\Users\marti/.ssh/id_rsa already exists.
Overwrite (y/n)? y
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\marti/.ssh/id_rsa.
Your public key has been saved in C:\Users\marti/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:onWG7Wt8U+k4ux0jgQkluR0VmEn4TBge/+HYCwnsMn4 marti@Pc-Martial
The key's randomart image is:
+---[RSA 4096]----+
|      +*o=o.     |
|     o+=*        |
|      =*...      |
|     ..=+O .     |
|    o = S =  .   |
|   . = = . oo    |
|    o E.. o+o    |
|     .  o.=o.o   |
|       ...o=.    |
+----[SHA256]-----+
```

```
C:\Users\marti> ssh admin@10.101.1.11
admin@10.101.1.11's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Sun Sep 26 20:50:44 2021 from 10.101.1.1
```

## II. Partitionnement

- Ajout de deux disques durs à la machine virtuelle, de 3Go chacun.

![](https://i.imgur.com/aaIG9bR.png)

- agréger les deux disques en un seul volume group

```
[admin@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for admin:
  Physical volume "/dev/sdb" successfully created.
[admin@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
[admin@node1 ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   3.00g 3.00g
  /dev/sdc      lvm2 ---   3.00g 3.00g
```
```
[admin@node1 ~]$ sudo vgcreate data /dev/sdb
  Volume group "data" successfully created
[admin@node1 ~]$ sudo vgextend data /dev/sdc
  Volume group "data" successfully extended
[admin@node1 ~]$ sudo vgs
  VG   #PV #LV #SN Attr   VSize  VFree
  data   2   0   0 wz--n-  5.99g 5.99g
  rl     1   2   0 wz--n- <7.00g    0
```
- créer 3 logical volumes de 1 Go chacun formater ces partitions en ext4

```
[admin@node1 ~]$ sudo lvcreate -L 1G data -n lv_data_1
  Logical volume "lv_data_1" created.
[admin@node1 ~]$ sudo lvcreate -L 1G data -n lv_data_2
  Logical volume "lv_data_2" created.
[admin@node1 ~]$ sudo lvcreate -L 1G data -n lv_data_3
  Logical volume "lv_data_3" created.
[admin@node1 ~]$ sudo lvs
  LV        VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  lv_data_1 data -wi-a-----   1.00g
  lv_data_2 data -wi-a-----   1.00g
  lv_data_3 data -wi-a-----   1.00g
  root      rl   -wi-ao----  <6.20g
  swap      rl   -wi-ao---- 820.00m
```

```
[admin@node1 ~]$ sudo mkfs -t ext4 /dev/data/lv_data_1
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: b3dade83-99da-42f3-9c83-b017fe4aae78
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

```
[admin@node1 ~]$ sudo mkfs -t ext4 /dev/data/lv_data_2
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: eb9aa6a7-2048-461c-9582-cc355ec32741
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```
```
[admin@node1 ~]$ sudo mkfs -t ext4 /dev/data/lv_data_3
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: b2f371ff-7488-4315-8335-7633fa708a4e
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```
- monter ces partitions pour qu'elles soient accessibles aux points de montage /mnt/part1, /mnt/part2 et /mnt/part3

```
[admin@node1 ~]$ sudo mkdir /mnt/part1
[admin@node1 ~]$ sudo mkdir /mnt/part2
[admin@node1 ~]$ sudo mkdir /mnt/part3
[admin@node1 ~]$ sudo mount /dev/data/lv_data_1 /mnt/part1
[admin@node1 ~]$ sudo mount /dev/data/lv_data_2 /mnt/part2
[admin@node1 ~]$ sudo mount /dev/data/lv_data_3 /mnt/part3
```

- Grâce au fichier /etc/fstab, faites en sorte que cette partition soit montée automatiquement au démarrage du système.

```
[admin@node1 ~]$ sudo nano /etc/fstab

/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=54c1c5d3-e7b8-49ce-84d7-a304a94eb447 /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
/dev/data/lv_data_1 /mnt/part1 ext4 defaults 0 0
/dev/data/lv_data_2 /mnt/part2 ext4 default 0 0
/dev/data/lv_data_3 /mnt/part3 ext4 default 0 0
```

## III. Gestion de services
### 1. Interaction avec un service existant

- Parmi les services système déjà installés sur CentOS, il existe firewalld. Cet utilitaire est l'outil de firewalling de CentOS. Assurez-vous que :

- l'unité est démarrée
```
[admin@node1 ~]$ systemctl is-active firewalld
active
```
- l'unitée est activée (elle se lance automatiquement au démarrage)
```
[admin@node1 ~]$ systemctl is-enabled firewalld
enabled
```

### 2. Création de service
#### A. Unité simpliste
- Créer un fichier qui définit une unité de service web.service dans le répertoire /etc/systemd/system

```
[admin@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
```

```
[admin@node1 system]$ sudo nano web.service

Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
```

- Une fois le service démarré, assurez-vous que pouvez accéder au serveur web : avec un navigateur ou la commande curl sur l'IP de la VM, port 8888.
```
PS C:\Users\marti> curl http://10.101.1.11:8888/


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                    <html>
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    <title>Directory listing fo...
RawContent        : HTTP/1.0 200 OK
                    Content-Length: 942
                    Content-Type: text/html; charset=utf-8
                    Date: Sun, 26 Sep 2021 23:14:27 GMT
                    Server: SimpleHTTP/0.6 Python/3.6.8

                    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//...
Forms             : {}
Headers           : {[Content-Length, 942], [Content-Type, text/html; charset=utf-8], [Date, Sun, 26 Sep 2021 23:14:27
                    GMT], [Server, SimpleHTTP/0.6 Python/3.6.8]}
Images            : {}
InputFields       : {}
Links             : {@{innerHTML=bin@; innerText=bin@; outerHTML=<A href="bin/">bin@</A>; outerText=bin@; tagName=A;
                    href=bin/}, @{innerHTML=boot/; innerText=boot/; outerHTML=<A href="boot/">boot/</A>;
                    outerText=boot/; tagName=A; href=boot/}, @{innerHTML=dev/; innerText=dev/; outerHTML=<A
                    href="dev/">dev/</A>; outerText=dev/; tagName=A; href=dev/}, @{innerHTML=etc/; innerText=etc/;
                    outerHTML=<A href="etc/">etc/</A>; outerText=etc/; tagName=A; href=etc/}...}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 942

```
#### B. Modification de l'unité

- Créer un utilisateur web.
```
[admin@node1 system] sudo useradd web -m -s /bin/bash
```
-  Vérifier le bon fonctionnement avec une commande curl
```
PS C:\Users\marti> curl http://10.101.1.11:8888/srv/webatest/test.txt


StatusCode        : 200
StatusDescription : OK
Content           : test 42

RawContent        : HTTP/1.0 200 OK
                    Content-Length: 8
                    Content-Type: text/plain
                    Date: Sun, 26 Sep 2021 23:37:46 GMT
                    Last-Modified: Sun, 26 Sep 2021 23:34:13 GMT
                    Server: SimpleHTTP/0.6 Python/3.6.8

                    test 42

Forms             : {}
Headers           : {[Content-Length, 8], [Content-Type, text/plain], [Date, Sun, 26 Sep 2021 23:37:46 GMT],
                    [Last-Modified, Sun, 26 Sep 2021 23:34:13 GMT]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 8

```