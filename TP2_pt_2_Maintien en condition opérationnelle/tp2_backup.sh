#!/bin/bash
# Simple Script Backup
# martial 25/10/21

# Usage
usage() {
    echo "Usage: $0 <destination> <dir_to_backup>"
}

if [[ -z $1 || -z $2 ]] ; then
  >&2 echo -e "vous devez donner une destination et un dossier a backup."
  >&2 echo
  usage
fi

# Global variables
destination=$1
dir_to_backup=$2
date=$(date +tp2_backup_%y%m%d_%H%M%S.tar.gz)
path="$(pwd)/${date}"

# Check directory
if [[ ! -d $destination ]] ; then
  >&2 echo "destination \"${destination}\" n'est pas accessible."
  exit 1
fi

if [[ ! -d $dir_to_backup ]] ; then
  >&2 echo "dossier a backup \"${dir_to_backup}\" n'est pas accessible."
  exit 1
fi

# crée une archive de dir_to_backup
tar cvzf "${path}" "${dir_to_backup}" 1> /dev/null
if [[ $? -eq 0 ]] ; then
  echo "archive ${date} a été crée"
else
  >&2 echo
  >&2 echo "Erreur : Échec lors de l'archivage"
  exit 1
fi


# deplace l'archive vers la destination
rsync -av --remove-source-files "${path}" "${destination}" 1> /dev/null
if [[ $? -eq 0 ]] ; then
  echo "l'archive ${date} a été déplacer vers ${destination}"
else
  >&2 echo
  >&2 echo "Erreur : echec du déplacement ${date}"
  exit 1
fi

clean_backup() {
  # Conserve 5 sauvegardes par défaut
  if [[ -z $qty_to_keep ]] ; then qty_to_keep=5 ; fi

  qty_to_keep_tail=$((qty_to_keep+1))
  ls -tp "${destination}" | grep -v '/$' | tail -n +${qty_to_keep_tail} | xargs -I {} rm -- ${destination}/{}
  status=$?
  if [[ $status -eq 0 ]] ; then
    echo -e "${G}${B}[OK]${N} Dossier ${destination} nettoyer pour garder seulement les ${qty_to_keep} sauvegardes les plus récentes."
  else
    >&2 echo -e "${R}${B}[ERROR]${N} echec du nettoyage de ${destination}."
    exit 1
  fi
}

clean_backup 5
