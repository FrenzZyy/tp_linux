# TP2 pt. 2 : Maintien en condition opérationnelle
## 2. Setup

🌞 **Setup Netdata**
```
[admin@web ~]$ sudo su -
[sudo] password for admin:
[root@web ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
```

🌞 **Manipulation du *service* Netdata**

 ```
[admin@web ~]$ sudo systemctl enable netdata
[admin@web ~]$ sudo systemctl is-enabled netdata
enabled
```

- déterminer à l'aide d'une commande `ss` sur quel port Netdata écoute

cmd : `sudo ss -tulpn | grep netdata`

| state  | Recv-Q | Send-Q | Local Address:Port | Process |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| LISTEN | 0      | 128        | `0.0.0.0:19999`       | users:(("netdata",pid=29470,fd=5))
| LISTEN        |    0    |      128      |   `[::]:19999` | users:(("netdata",pid=29470,fd=6))   

- autoriser ce port dans le firewall

```
[admin@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[admin@web ~]$ sudo firewall-cmd --reload
success
[admin@web ~]$ sudo firewall-cmd --list-all
public (active)
{...}
  ports: 80/tcp 19999/tcp
{...}
```

🌞 **Setup Alerting**

```
[admin@backup ~]$ sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf

# discord (discordapp.com) global notification options

# multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/902190313894977577/yOzOmnuQJh_CNFHImPI-ZYJGVHUEfJvo7xWmqGeph1RTGX_akvoelS7G1SPmljfekoLT"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```

```
[admin@backup ~]$ sudo su -s /bin/bash netdata

bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1

bash-4.4$ /opt/netdata//usr/libexec/netdata/plugins.d/alarm-notify.sh test
```
![](https://i.imgur.com/HTYOHrP.png)

🌞 **Config alerting**
```
[admin@web netdata]$ sudo /opt/netdata/etc/netdata/edit-config health.d/ram.conf
{...}
warn: $this > (($status >= $WARNING)  ? (50) : (90))
{...}
```
```
[admin@web netdata]$ stress --vm 1 --vm-bytes 1024M```
```
![](https://i.imgur.com/04veffV.png)

# II. Backup

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | ?           | ?             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | ?           | ?             |

## 2. Partage NFS

🌞 **Setup environnement**

```
[admin@backup srv]$ mkdir backups
[admin@backup srv]$ cd backups/
[admin@backup backups]$ sudo mkdir web.tp2.linux
[admin@backup backups]$ ls
web.tp2.linux
```

🌞 **Setup partage NFS**

```
[admin@backup backups]$ sudo dnf -y install nfs-utils
[admin@backup backups]$ sudo nano /etc/idmapd.conf

[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = tp2.linux
```
```
[admin@backup backups]$ sudo nano /etc/exports

# create new
# for example, set [/home/nfsshare] as NFS share
/srv/backups/web.tp2.linux 10.102.1.0/24(rw,no_root_squash)
```
```
[admin@backup ~]$ sudo systemctl start nfs-server.service
```

🌞 **Setup points de montage sur `web.tp2.linux`**

- [sur le même site, y'a ça](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=2)
- monter le dossier `/srv/backups/web.tp2.linux` du serveur NFS dans le dossier `/srv/backup/` du serveur Web

```
[admin@web srv]$ sudo mount -t nfs 10.102.1.13:/srv/backups/web.tp2.linux /srv/backups
[admin@web srv]$ df -hT
Filesystem                             Type      Size  Used Avail Use% Mounted on
devtmpfs                               devtmpfs  387M     0  387M   0% /dev
tmpfs                                  tmpfs     405M  340K  405M   1% /dev/shm
tmpfs                                  tmpfs     405M  5.6M  400M   2% /run
tmpfs                                  tmpfs     405M     0  405M   0% /sys/fs/cgroup
/dev/mapper/rl-root                    xfs       6.2G  3.4G  2.9G  55% /
/dev/sda1                              xfs      1014M  241M  774M  24% /boot
tmpfs                                  tmpfs      81M     0   81M   0% /run/user/1000
10.102.1.13:/srv/backups/web.tp2.linux nfs4      6.2G  2.2G  4.0G  36% /srv/backups
```

```
[admin@web backups]$ sudo touch test
[admin@web backups]$ ls
test
```

```
[admin@backup web.tp2.linux]$ ls
test
```

```
[admin@web backups]$ sudo nano /etc/fstab
10.102.1.13:/srv/backups/web.tp2.linux /srv/backups                     nfs     defaults        00
```

## 3. Backup de fichiers

🌞 **Tester le bon fonctionnement**

```
[admin@backup srv]$ sudo ./tp2_backup.sh backups/ /tmp/test/
tar: Removing leading `/' from member names
archive tp2_backup_211026_112935.tar.gz a été crée
l'archive tp2_backup_211026_112935.tar.gz a été déplacer vers backups/
[OK] Dossier backups/ nettoyer pour garder seulement les 5 sauvegardes les plus récentes.

[admin@backup srv]$ cd backups/
[admin@backup backups]$ ls
tp2_backup_211026_112935.tar.gz  web.tp2.linux
```

```
[admin@backup backups]$ sudo tar -xf tp2_backup_211026_112935.tar.gz

[admin@backup backups]$ ls
tmp  tp2_backup_211026_112935.tar.gz  web.tp2.linux

[admin@backup backups]$ cd tmp/
[admin@backup tmp]$ ls
test
[admin@backup tmp]$ cd test/
[admin@backup test]$ ls
test1
```

## 4. Unité de service

### A. Unité de service

🌞 **Créer une *unité de service*** pour notre backup

```
[admin@backup system]$ sudo nano tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/bin/bash /srv/tp2_backup.sh /srv/backups/ /tmp/test
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=default.target
```
🌞 **Tester le bon fonctionnement**

```
[admin@backup system]$ sudo systemctl daemon-reload
[admin@backup system]$ sudo systemctl start tp2_backup.service
[admin@backup system]$ sudo systemctl status tp2_backup.service

{...}
Oct 26 12:03:41 backup.tp2.linux systemd[1]: Starting Our own lil backup service (TP2)...
Oct 26 12:03:41 backup.tp2.linux bash[2549]: tar: Removing leading `/' from member names
Oct 26 12:03:41 backup.tp2.linux bash[2549]: archive tp2_backup_211026_120341.tar.gz a été crée
Oct 26 12:03:41 backup.tp2.linux bash[2549]: l'archive tp2_backup_211026_120341.tar.gz a été déplacer vers /srv/backups/
Oct 26 12:03:41 backup.tp2.linux bash[2549]: [OK] Dossier /srv/backups/ nettoyer pour garder seulement les 5 sauvegarde>
Oct 26 12:03:41 backup.tp2.linux systemd[1]: tp2_backup.service: Succeeded.
Oct 26 12:03:41 backup.tp2.linux systemd[1]: Started Our own lil backup service (TP2).

[admin@backup system]$ cd /srv/backups/
[admin@backup backups]$ ls
tp2_backup_211026_112935.tar.gz  tp2_backup_211026_120341.tar.gz  web.tp2.linux
```


### B. Timer

- **prouver que... le timer est actif actuellement qu'il est paramétré pour être actif dès que le système boot**

```
[admin@web backup]$ sudo systemctl status tp2_backup.timer

tp2_backup.timer - Periodically run our TP2 backup script
   Loaded: loaded (/etc/systemd/system/tp2_backup.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Tue 2021-10-12 04:08:12 CEST; 34s ago
  Trigger: Tue 2021-10-12 04:09:00 CEST; 12s left

Oct 12 04:08:12 web.tp2.linux systemd[1]: Started Periodically run our TP2 backup script.
```
```
[admin@web backup]$ sudo systemctl is-enabled tp2_backup.timer
enabled
```

- **vérifiez que la backup s'exécute correctement**
```
[admin@backup web.tp2.linux]$ ls -l
total 28
-rw-r--r--. 1 root root 212 Oct 26 16:00 tp2_backup_20211012_040048.tar.gz
-rw-r--r--. 1 root root 215 Oct 26 16:04 tp2_backup_20211012_040423.tar.gz
-rw-r--r--. 1 root root 215 Oct 26 16:08 tp2_backup_20211012_040812.tar.gz
-rw-r--r--. 1 root root 215 Oct 26 16:09 tp2_backup_20211012_040903.tar.gz
-rw-r--r--. 1 root root 215 Oct 26 16:10 tp2_backup_20211012_041003.tar.gz
-rw-r--r--. 1 root root 215 Oct 26 16:11 tp2_backup_20211012_041103.tar.gz
-rw-r--r--. 1 root root 215 Oct 26 16:12 tp2_backup_20211012_041203.tar.gz
```

### C. Contexte

- **prouvez avec la commande sudo systemctl list-timers que votre service va bien s'exécuter la prochaine fois qu'il sera 03h15**

```
[admin@web srv]$ sudo systemctl list-timers
NEXT                          LEFT     LAST                          PASSED    UNIT                         ACTIVATES
Fri 2021-10-15 03:15:00 CEST  8h left  n/a                           n/a       tp2_backup.timer             tp2_backup.>
Fri 2021-10-15 17:57:04 CEST  23h left Thu 2021-10-26 17:57:04 CEST  19min ago systemd-tmpfiles-clean.timer systemd-tmp>
n/a                           n/a      n/a                           n/a       dnf-makecache.timer          dnf-makecac>
```
```
Fichier /etc/systemd/system/tp2_backup.timer

[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* 3:15:00

[Install]
WantedBy=timers.target

Fichier /etc/systemd/system/tp2_backup.service

[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/backup/ /var/www/sub-domains/com.yourdomain.nextcloud
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

# III. Reverse Proxy

## 2. Setup simple

### Installer NGINX
```
[admin@front ~]$ sudo dnf install -y epel-release
[admin@front ~]$ sudo dnf install -y nginx
```
### Tester

- **Lancer le service nginx**

```
[admin@front ~]$ sudo start nginx
[admin@front ~]$ sudo systemctl enable nginx
```
- **repérer le port qu'utilise NGINX par défaut, pour l'ouvrir dans le firewall**

```
[admin@front ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```
- **vérifier que vous pouvez joindre NGINX avec une commande curl depuis votre PC**

```
PS C:\Users\marti> curl http://10.102.1.14/                                                                       

StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

                    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
                      <head>
                        <title>Test Page for the Nginx...
```
### Explorer la conf par défaut de NGINX

- repérez l'utilisateur qu'utilise NGINX par défaut

```
[admin@front ~]$ sudo cat /etc/nginx/nginx.conf
[sudo] password for admin:
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
```

- nginx utilisie l'ulisateur nginx par defaut


- par défaut, le fichier de conf principal inclut d'autres fichiers de conf mettez en évidence ces lignes d'inclusion dans le fichier de conf principal

```
include /etc/nginx/default.d/*.conf;
```
### Modifier la conf de NGINX

- créer un fichier /etc/nginx/conf.d/web.tp2.linux.conf avec le contenu suivant

```
[admin@front ~]$ sudo cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
    # on demande à NGINX d'écouter sur le port 80 pour notre NextCloud
    listen 80;

    # ici, c'est le nom de domaine utilisé pour joindre l'application
    # ce n'est pas le nom du reverse proxy, mais le nom que les clients devront saisir pour atteindre le site
    server_name web.tp2.linux; # ici, c'est le nom de domaine utilisé pour joindre l'application (pas forcéme

    # on définit un comportement quand la personne visite la racine du site (http://web.tp2.linux/)
    location / {
        # on renvoie tout le trafic vers la machine web.tp2.linux
        proxy_pass http://web.tp2.linux;
    }
}
```
# IV. Firewalling

## Mise en place

### A. Base de données

- Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd

```
[admin@db ~]$ sudo firewall-cmd --get-active-zones
db
  sources: 10.102.1.11/32
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/32
[admin@db ~]$ sudo firewall-cmd --get-default-zone
drop
```
### B. Serveur Web

- Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd

```
[admin@web ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
proxy
  sources: 10.102.1.14/32
ssh
  sources: 10.102.1.1/32
[admin@web ~]$ sudo firewall-cmd --get-default-zone
drop
```
### C. Serveur de backup

- Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd

```
[admin@backup ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
nfs
  sources: 10.102.1.11/32 10.102.1.12/32
ssh
  sources: 10.102.1.1/32
[admin@backup ~]$ sudo firewall-cmd --get-default-zone
drop
[admin@backup ~]$
```

### D. Reverse Proxy

- Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd

```
[admin@front ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
net
  sources: 10.102.1.0/24
ssh
  sources: 10.102.1.1/32
[admin@front ~]$ sudo firewall-cmd --get-default-zone
drop
[admin@front ~]$
```

### E. Tableau récap

🌞 **Rendez-moi le tableau suivant, correctement rempli :**

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | `80` `22`           | `10.102.1.1` `10.102.1.14`             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | `3306` `22`          | `10.102.1.1` `10.102.1.11`            |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | `2049` `22`          | `10.102.1.1` `10.102.1.11` `10.102.1.12`            |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | `80` `22`           | `10.102.1.1` `10.102.1.0/24`             |